
// Blocco dichiarazioni
const int digitalPin = 7;
const int analogPinAlimentato = 0;
const int analogPinRicevente = 1;
const int points = 50; // Quanti punti sperimentali acquisire

int VA[points]; 
int VR[points]; 
long tA[points]; // Definisice l'array t come intero long
long tR[points];
long startTime; // Definisce il valore StartTime come intero long 

int start = 0; // Definisce il valore start (usato come flag)
int i; // Definisice la variabile intera i (contatore)

void setup() {
    Serial.begin(19200); // Inizializza la porta seriale a 9600 baud

    pinMode(digitalPin, OUTPUT);
    digitalWrite(digitalPin, LOW);
    
    // Clock digitalizzatore
    bitClear(ADCSRA,ADPS0);
    bitSet(ADCSRA,ADPS1);
    bitClear(ADCSRA,ADPS2);
}
  

void loop() {
    if (Serial.available() > 0) {
        Serial.flush(); // Svuota la seriale
        start = 1; // Pone il flag start a uno
        
    } 
      
    if(!start) return;
                    
    delay(2000); // Delay per essere sicuro che tutto funzioni

    // Pulisce i pin analogici
    for (i = 0; i < 100; i++) {
        analogRead(analogPinAlimentato);
        analogRead(analogPinRicevente);
    }
    
    startTime = micros();
    digitalWrite(digitalPin, HIGH);
    for (i = 0; i < points; i++) {
        tA[i]=micros() - startTime;
        VA[i]=analogRead(analogPinAlimentato);
        tR[i]=micros() - startTime;
        VR[i]=analogRead(analogPinRicevente);
        
    }

    // print stampa, printl va anche a capo
    for(i = 0; i < points; i++) {
        Serial.print(tA[i]);
        Serial.print(" ");
        Serial.print(VA[i]);
        Serial.print(" ");
        Serial.print(tR[i]);
        Serial.print(" ");
        Serial.println(VR[i]);
    }
    
    start = 0;
}
