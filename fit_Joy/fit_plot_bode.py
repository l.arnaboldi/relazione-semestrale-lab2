#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Prima di usare il programma ricorda di modificare:
- il file di input
- gli errori in input
- eventuale preprocessing dei dati
- il modello per curvefit e quello per ODR
- l'array dei parametri iniziali
- figwidth, figheight, elinewidth, markersize nei plot
- se vuoi mettere questo codice nel .tex,
	leva ogni riferimento a ODR, ovvero
	qui e nella sezione apposita
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy import odr
import matplotlib.ticker as tck
from scipy.stats import chi2

#incertezze dati
f_err =[0.001,0.01,0.1,0.1,0.1,0.1,0.1,0.1,0.1,1,1,10,10,10]

#input dati
f_data, A_in_data,A_in_err,A_out_data,A_out_err, S_data, S_err = np.loadtxt("dati_definitivi\sabrina_oscilloscopio.txt", unpack=True,skiprows=0,comments='#')
if (len(f_data) != len(f_err)): print ("WARNING: data length!")

#preprocessing, definizioni extra & co
S_data_help=S_data
S_data=2*np.pi*f_data*S_data
S_err=S_data*np.sqrt((S_err/S_data_help)**2+(f_err/f_data)**2)

Att_data=A_out_data/A_in_data
Att_err= Att_data*np.sqrt((A_in_err/A_in_data)**2+(A_out_err/A_out_data)**2)

################### CURVE_FIT #####################
'''
#modello per curvefit
def model1 (x, p0, p1): #parametri singoli!
	return p0 + p1 * x
def model2 (x,p0,p1,p2):
	return 1

#calcolo del chi2
def chi2(x_data,y_data,popt):
	return np.sum(((y_data - model(x_data,*popt))/y_err)**2)

#fit con curvefit
startParams	= np.array([1,1])
popt, pcov 	= curve_fit(model, x_data, y_data, p0 = startParams, sigma = y_err, absolute_sigma = True)
pstdev 		= np.sqrt(pcov.diagonal())

#output di curvefit
print("I parametri di best fit sono: ", popt)
print("I rispettivi errori sono:     ", pstdev)
print("\nLa matrice di covarianza è: ")
print(pcov)

#calcolo e output del chi2
chisq = chi2(x_data,y_data,popt)
chisq_exp = len(x_data) - len(popt)
chisq_tol = np.sqrt(2 * (len(x_data) - len(popt)))
print("\nIl chi2 calcolato è: ",)
print(chisq)
print("Il chi2 atteso è: ", chisq_exp, " +- ", chisq_tol)
print("Sei entro %f volte sqrt(2v) dal chi2 atteso" %(abs(chisq-chisq_exp)/chisq_tol) )

def pval(chi2,chi2exp):
	if (chi2 > chi2exp): 
		return -1


###################### ODR ########################
def ODRtype_model ( p, x ):
	return p[0] + p[1] * x
print("\nFIT CON ODR")
ODR_model	= odr.Model(ODRtype_model)
ODR_data	= odr.RealData(x_data, y = y_data, sx = x_err, sy = y_err)
ODR_fit		= odr.ODR(ODR_data, ODR_model, beta0 = startParams)
ODR_out		= ODR_fit.run()
ODR_out.pprint()
print("Orthogonal chi2: ", ODR_out.sum_square)
'''
###################### PLOT ########################
plt.errorbar(f_data,Att_data,Att_err,f_err,fmt='none')
plt.xscale('log')

plt.show()





'''
#Conversione
def cm2in (x):
	return x *0.3937

#Definizione della figura
figwidth = cm2in(16)
figheight = cm2in(14)
left, width = 0.1, 0.8
bottom_res, height_res = 0.1, 0.2
bottom_fit = bottom_res + height_res + 0.0
height_fit = 1 - 0.1 - bottom_fit
plt.figure(1, figsize=(figwidth, figheight))


#Definizione delle due aree del grafico
rect_fit = [left, bottom_fit, width, height_fit]
rect_res = [left, bottom_res, width, height_res]
axRes = plt.axes(rect_res)
axFit = plt.axes(rect_fit, sharex=axRes)

#Tick
plt.setp(axFit.get_xticklabels(), visible=False)
axFit.tick_params(axis='both',direction='in',top=True,right=True,grid_linestyle='dotted',grid_linewidth=0.5)
axRes.tick_params(axis='both',direction='in',top=True,right=True,grid_linestyle='dotted',grid_linewidth=0.5)

#Testo
plt.sca(axRes)
plt.xlabel("testx")
plt.sca(axRes)
plt.ylabel("Residuo / $\sigma_y$")
plt.sca(axFit)
plt.ylabel("testfit")
plt.suptitle("titolo")

#Plot di dati sperimentali e fit
plt.sca(axFit)
plt.errorbar(x_data, y_data, xerr=x_err, yerr=y_err, fmt='.', color='blue', lw=0, elinewidth = 1., markersize = 4)
X = np.linspace(*plt.xlim(),len(x_data)*10)
plt.plot(X,model(X,*popt), color='black')
plt.grid(True)

#Plot di residui e linea base
plt.sca(axRes)
#~ plt.errorbar(x_data, y_data-model(x_data,*popt), xerr=0, yerr=y_err, fmt='.', color='blue', lw=0, elinewidth = 1., markersize = 4)
plt.errorbar(x_data, (y_data-model(x_data,*popt))/y_err, xerr=0, yerr=0, fmt='.', color='blue', lw=0, elinewidth = 1., markersize = 4)
plt.plot(X,X*[0], color='black')

#Pretty it up
plt.grid(True)
plt.sca(axRes)
resBounds = np.amax(np.abs(plt.ylim()))
plt.ylim([-resBounds,resBounds])

#Output
plt.savefig("test.png",dpi = 300)
plt.show()
plt.close()

#varie ed eventuali
#~ axRes.xaxis.set_major_formatter(tck.LogLocator(base=10,subs=(1.0,2.0,3.0,4.0)))
'''