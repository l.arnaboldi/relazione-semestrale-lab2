#!/usr/bin/env python
# -*- coding: utf-8 -*-

#p1=1.6
p2=217
"""
Prima di usare il programma ricorda di modificare:
- il file di input
- gli errori in input
- eventuale preprocessing dei dati
- il modello per curvefit e quello per ODR
- l'array dei parametri iniziali
- figwidth, figheight, elinewidth, markersize nei plot
- se vuoi mettere questo codice nel .tex,
	leva ogni riferimento a ODR, ovvero
	qui e nella sezione apposita
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy import odr
import matplotlib.ticker as tck
from scipy.stats import chi2

#input dati
x_data, y_data = np.loadtxt("dati_definitivi\carica_sabrina_1.txt", unpack=True,skiprows=0,comments='#')
if (len(x_data) != len(y_data)): print ("WARNING: data length!")

#confersione digit->volt
def conversione (x,a,b):
	return a*x+b 

def conversione_errori (x,a):
	return x*a

y_data = conversione(y_data,0.00504042,0.00018591)
x_data=x_data*(10**(-6))

#preprocessing, definizioni extra & co

#incertezze dati
x_err = np.full(len(x_data),1*(10**(-6)))
y_err = np.full(len(y_data),0.00504042*1)
y_err_t =y_err

################### CURVE_FIT #######################
#modello per curvefit
def model (x, p0,p3,p1):#p2): #parametri singoli!
	return p0*((p1/(p1+p2))*(1-np.exp(-x/(p3/(p2+p1))))+np.exp(-x/(p3/(p2+p1))))

#derivata del modello per il fit iterato
def d_model (x, p0,p3,p1):#p2):
	return -p0*np.exp(-x/(p3/(p2+p1)))*(p2)/p3
	
#calcolo del chi2
def chi2(x_data,y_data,popt):
	return np.sum(((y_data - model(x_data,*popt))/y_err)**2)

#fit con curvefit
startParams	= np.array([5.15346584,0.0002539,1])
popt, pcov 	= curve_fit(model, x_data, y_data, p0 = startParams, sigma = y_err, absolute_sigma = False)
pstdev 		= np.sqrt(pcov.diagonal())

#output di curvefit
print("I parametri di best fit sono: ", popt)
print("I rispettivi errori sono:     ", pstdev)
print("\nLa matrice di covarianza è: ")
print(pcov)

#calcolo e output del chi2
chisq = chi2(x_data,y_data,popt)
chisq_exp = len(x_data) - len(popt)
chisq_tol = np.sqrt(2 * (len(x_data) - len(popt)))
print("\nIl chi2 calcolato è: ",)
print(chisq)
print("Il chi2 atteso è: ", chisq_exp, " +- ", chisq_tol)
print("Sei entro %f volte sqrt(2v) dal chi2 atteso" %(abs(chisq-chisq_exp)/chisq_tol) )

def pval(chi2,chi2exp):
	if (chi2 > chi2exp): 
		return -1

##################### FIT ITERATO #################
i=0
for i in range (0,10):
	
	#trasporto errori
	if (i==0):
		y_err_t=np.sqrt(y_err**2 +(d_model(x_data,*popt)*x_err)**2)
	else :
		y_err_t=np.sqrt(y_err**2 +(d_model(x_data,*i_popt)*x_err)**2)
	
	#fit con curvefit
	if (i==0):
		startParams	= np.array([*popt])
	else :
		startParams	= np.array([*i_popt])
		
	i_popt, i_pcov 	= curve_fit(model, x_data, y_data, p0 = startParams, sigma = y_err_t, absolute_sigma = False)
	i_pstdev 		= np.sqrt(i_pcov.diagonal())

	#output di curvefit
	print("I parametri di best fit sono: ", i_popt)
	i=i+1



'''
###################### ODR ########################
def ODRtype_model ( p, x ):
	return p[0] + p[1] * x
print("\nFIT CON ODR")
ODR_model	= odr.Model(ODRtype_model)
ODR_data	= odr.RealData(x_data, y = y_data, sx = x_err, sy = y_err)
ODR_fit		= odr.ODR(ODR_data, ODR_model, beta0 = startParams)
ODR_out		= ODR_fit.run()
ODR_out.pprint()
print("Orthogonal chi2: ", ODR_out.sum_square)
'''
###################### PLOT ########################



plt.errorbar(x_data,y_data,y_err_t,x_err,fmt='none',capsize=4 )

X=np.linspace(0,0.00035,1000)
plt.plot(X,model(X,*popt))
plt.plot(X,model(X,*i_popt),color='red')
plt.show()

plt.show()

'''
#Conversione
def cm2in (x):
	return x *0.3937

#Definizione della figura
figwidth = cm2in(16)
figheight = cm2in(14)
left, width = 0.1, 0.8
bottom_res, height_res = 0.1, 0.2
bottom_fit = bottom_res + height_res + 0.0
height_fit = 1 - 0.1 - bottom_fit
plt.figure(1, figsize=(figwidth, figheight))


#Definizione delle due aree del grafico
rect_fit = [left, bottom_fit, width, height_fit]
rect_res = [left, bottom_res, width, height_res]
axRes = plt.axes(rect_res)
axFit = plt.axes(rect_fit, sharex=axRes)

#Tick
plt.setp(axFit.get_xticklabels(), visible=False)
axFit.tick_params(axis='both',direction='in',top=True,right=True,grid_linestyle='dotted',grid_linewidth=0.5)
axRes.tick_params(axis='both',direction='in',top=True,right=True,grid_linestyle='dotted',grid_linewidth=0.5)

#Testo
plt.sca(axRes)
plt.xlabel("testx")
plt.sca(axRes)
plt.ylabel("Residuo / $\sigma_y$")
plt.sca(axFit)
plt.ylabel("testfit")
plt.suptitle("titolo")

#Plot di dati sperimentali e fit
plt.sca(axFit)
plt.errorbar(x_data, y_data, xerr=x_err, yerr=y_err, fmt='.', color='blue', lw=0, elinewidth = 1., markersize = 4)
X = np.linspace(*plt.xlim(),len(x_data)*10)
plt.plot(X,model(X,*popt), color='black')
plt.grid(True)

#Plot di residui e linea base
plt.sca(axRes)
#~ plt.errorbar(x_data, y_data-model(x_data,*popt), xerr=0, yerr=y_err, fmt='.', color='blue', lw=0, elinewidth = 1., markersize = 4)
plt.errorbar(x_data, (y_data-model(x_data,*popt))/y_err, xerr=0, yerr=0, fmt='.', color='blue', lw=0, elinewidth = 1., markersize = 4)
plt.plot(X,X*[0], color='black')

#Pretty it up
plt.grid(True)
plt.sca(axRes)
resBounds = np.amax(np.abs(plt.ylim()))
plt.ylim([-resBounds,resBounds])

#Output
plt.savefig("test.png",dpi = 300)
plt.show()
plt.close()

#varie ed eventuali
#~ axRes.xaxis.set_major_formatter(tck.LogLocator(base=10,subs=(1.0,2.0,3.0,4.0)))
'''