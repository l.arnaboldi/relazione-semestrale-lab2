from lab_tool2 import *

filename_amp = 'Elaborato/immagini/fit_bode_ampiezze.pdf'
filename_sfa = 'Elaborato/immagini/fit_bode_sfasamento.pdf'

R, dR = numpy.loadtxt('dati_definitivi/R.txt', unpack = True)
rg = numpy.loadtxt('dati_definitivi/rg.txt')
R_tot = R + rg
dR_tot = dR

# Parametri
# a = r / (R+r)
# b = (R + r) / L

def ampiezza(x, a, b):
	return 20 * numpy.log10( numpy.sqrt(((a*b)**2 + (2*numpy.pi*x)**2) / ((b**2 + (2*numpy.pi*x)**2))))
	
def d_ampiezza(x, L, r):
	return -(80 * numpy.pi**2 * (r**2 - (R+r)**2) * x) / ((((R+r)/L)**2 + 4 * numpy.pi**2 * x**2) * (r**2 + 4 * L**2 * numpy.pi**2 * x**2) * numpy.log(10.))
	
f, df, A_in, dA_in, A_out, dA_out, s, ds = numpy.loadtxt('dati_definitivi/sabrina_oscilloscopio.txt', unpack = True)

 
att = 20 * numpy.log10(A_out / A_in) # Converto in decibel
datt = (20 / numpy.log(10)) * (1 / (A_out / A_in)) *(dA_in / A_in + dA_out / A_out) * (A_out / A_in) # Errore

p_init = (8.e-3, 1.6)
p, cov = MLS(ampiezza, d_ampiezza, f, att, uy = datt, ux = df, p0 = p_init, absolute_sigma = False)

print("Parametri: ", p )
print("Chi quadro, ndof: ", chi2MLS(ampiezza, d_ampiezza, p, f, att, datt, df))
print("Covarianza:\n", cov)
print("Covarianza normalizzata:\n", normCov(cov))

plotter(ampiezza, p, f, att, datt, df, d_ampiezza, xlabel = '$f$ [Hz]', ylabel = '$\dfrac{A_{out}}{A_{in}}$ [dB]', figfile = filename_amp, xlogscale = True)

## Trovo r e L
print('RISULTATI')
r = p[0] / (1-p[0]) * R
dr = numpy.sqrt(2 * (numpy.sqrt(cov[0][0]) / p[0])**2 + (dR / R)**2) * r
print('r = ({0} +- {1}) Ohm'.format(r, dr))
L = 1 / (p[1] * (1-p[0])) * R
dL = (numpy.sqrt(cov[0][0]) / p[0] + numpy.sqrt(cov[1][1]) / p[1] + dR / R) * L
print('L = ({0} +- {1}) H'.format(L, dL))


print('\n\n\n\nSFASAMENTO')
## SFASAMENTO
def sfasamento(x, L, r):
	return numpy.arctan(((2*numpy.pi*x) * L * R ) / (r*(r+R) + (2*numpy.pi*x*L)**2))

phi = 2* numpy.pi * s * f
dphi = 2* numpy.pi * ds * f
phi[5] = numpy.pi - phi[5] # Punto preso al contarrio
p_init = (0.009, 1.6)
p, cov = curve_fit(sfasamento, f, phi, sigma = dphi, p0 = p_init, absolute_sigma = False, bounds = ((0., 0.), (1., 10.)))

print("Parametri: ", p )
print("Chi quadro, ndof: ", chi2LS(sfasamento, f, phi, p, dphi))
print("Covarianza:\n", cov)
print("Covarianza normalizzata:\n", normCov(cov))

plotter(sfasamento, p, f, phi, dphi, xlabel = '$f$ [Hz]', ylabel = '$tan(\Delta \varphi )$', figfile = filename_sfa, xlogscale = True)

