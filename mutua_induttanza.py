
import serial
import time
import numpy
import matplotlib.pyplot as plt

serialDevice = '/dev/cu.usbmodem14201'


print('Programma Avviato')

ard = serial.Serial(serialDevice, 19200)
time.sleep(2) # delay affinchè la seriale si configuri per bene

print('Mando segnale Start')
ard.write(b's') # scrivo sulla seriale per far iniziare l'acquisizione
time.sleep(2) # delay per essere sicuro che il messaggio sia ricevuto 

print('Leggo i dati dalla seriale')
cnt = 50
tA = numpy.zeros(cnt)
tR = numpy.zeros(cnt)
VA = numpy.zeros(cnt)
VR = numpy.zeros(cnt)
for i in range(0, cnt):
    data = ard.readline().decode('utf-8')
    if data:
        tas, vas, trs, vrs = data[:-1].split(' ')
        print(data)
        tA[i] = float(tas)
        tR[i] = float(trs)
        VA[i] = float(vas)
        VR[i] = float(vrs)
ard.close() # chiude la comunicazione seriale con Arduino

plt.plot(tA, VA, linestyle = '', marker = 'o', color = 'red')
plt.plot(tR, VR, linestyle = '', marker = 'o', color = 'green')
plt.show()

filename = 'mutua.txt'

with open('data_tmp/' + filename, 'w+') as output:
	for i in range(0, cnt):
		output.write('{0} {1} {2} {3}\n'.format(tA[i], VA[i], tR[i], VR[i]))
 
print('Finito') # scrive sulla console che ha finito
