
import serial
import time
import numpy


FileName = 'data.txt'
Directory = 'data_tmp/'
outputFile = open(Directory + FileName, "w")
serialDevice = '/dev/cu.usbmodem14201'


print('Programma Avviato')

ard = serial.Serial(serialDevice, 19200)
time.sleep(2) # delay affinchè la seriale si configuri per bene

print('Mando segnale Start')
ard.write(b's') # scrivo sulla seriale per far iniziare l'acquisizione
time.sleep(2) # delay per essere sicuro che il messaggio sia ricevuto 

print('Leggo i dati dalla seriale')
cnt = 20
while cnt > 0:
    data = ard.readline().decode('utf-8')
    if data:
        outputFile.write(data)
        print(data)
    cnt -= 1
   
ard.close() # chiude la comunicazione seriale con Arduino            
outputFile.close()
print('Finito') # scrive sulla console che ha finito
