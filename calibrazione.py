
import serial
import time
import numpy
import matplotlib.pyplot as plt
import lab_tool

serialDevice = '/dev/cu.usbmodem14201'
filename = 'calibrazione.txt'

print('Programma Avviato')

ard = serial.Serial(serialDevice, 19200)
time.sleep(2) # delay affinchè la seriale si configuri per bene

print('Mando segnale Start')
ard.write(b's') # scrivo sulla seriale per far iniziare l'acquisizione
print('Eseguo...')
time.sleep(5) # delay per essere sicuro che il messaggio sia ricevuto


print('Leggo i dati dalla seriale')
cnt = 256
x = numpy.zeros(cnt)
for i in range(0, cnt):
    data = ard.readline().decode('utf-8')
    if data:
        x[i] = float(data[:-1])
        
ard.close() # chiude la comunicazione seriale con Arduino

V = lab_tool.stat_value(x)
print('Ho misurato: {}'.format(V))
VV = float(input('Volmetro: '))
digit = float(input('Digit: '))

with open(filename, 'a+') as output:
	output.write('{0}\t{1}\t{2}\t{3}\n'.format(V.n, V.s, VV, numpy.sqrt((VV*0.005)**2 + (2*digit)**2)))
 
print('Finito') # scrive sulla console che ha finito
