
\documentclass[]{article}
\usepackage[latin1]{inputenc}
\usepackage[italian]{babel}
\usepackage{amsmath}
\usepackage{circuitikz}
\usepackage{tikz}
\usepackage{siunitx}
\usepackage{listings}
\usepackage{hyperref}
\usepackage[margin=1.in]{geometry} % Margini


\lstdefinestyle{shared}{
	belowcaptionskip=1\baselineskip,
	breaklines=true,
	xleftmargin=\parindent,
	showstringspaces=false,
	basicstyle=\fontsize{6.5}{7.5}\ttfamily,
}
\lstdefinestyle{cpp}{
	style=shared,
	language=C++,
	keywordstyle=\bfseries\color{green!40!black},
	commentstyle=\itshape\color{red!80!black},
	identifierstyle=\color{blue},
	stringstyle=\color{purple!40!black},
}
\lstdefinestyle{py}{
	style=shared,
	language=Python,
	keywordstyle=\bfseries\color{green!40!black},
	commentstyle=\itshape\color{purple!40!black},
	identifierstyle=\color{blue},
	stringstyle=\color{orange},
}

\title{Caratterizzazione di una bobina toroidale\footnote{La repository del progetto si trova nella pagina web \url{https://gitlab.com/l.arnaboldi/relazione-semestrale-lab2}.}}
\author{Luca Arnaboldi\thanks{\href{mailto:luca.arnaboldi@arn4.it}{luca.arnaboldi@arn4.it}, matricola: 565650}\\ 
	Jacopo Joy Colombini\thanks{\href{mailto:jacopojoy@hotmail.it}{jacopojoy@hotmail.it}, matricola: 566376}\\ 
	Veronica Sacchi\thanks{\href{mailto:ver22albireo@gmail.com}{ver22albireo@gmail.com}, matricola: 566308}}

\begin{document}

\maketitle
\abstract{Nell'esperienza di seguito descritta ci si propone di caratterizzare i parametri circuitali di una bobina toroidale da noi costruita, modellizzata come un'induttanza ideale e una resistenza poste in serie. A tal fine si sono svolti due tipi di esperimenti: la misura del tempo caratteristico di un circuito RL e lo studio della funzione di trasferimento in uscita ai capi della bobina.}

\section*{Cenni teorici}
Nel nostro caso si \`e schematizzata la bobina utilizzata come un'induttanza ideale $L$ in serie a una resistenza $r$. Inoltre alla bobina � stato aggiunto in serie un resitore $R$. Lo schemma del circuito � rappresentato in Figura~\ref{fig:schemacircuito}.

\begin{figure}[!ht]
	\centering
	\begin{circuitikz}
		\draw(0,1)node[anchor=east]{$V_{in}$}to[short, o-](1,1)to[R=$R$](2.5,1)to[short, -o](5,1)node[anchor=west]{$V_{out}$};
		\draw(3.5,1)to[R=$r$](3.5,-1)to[L=$L$](3.5,-3)node[ground]{};
		\draw(0,-3)to[short, o-o](5,-3);
	\end{circuitikz}
	\caption{schema circuitale dell'esperienza.}
	\label{fig:schemacircuito}
\end{figure}
Analizziamo due situazioni differenti, la prima delle quali vede $V_{in}$ costante nel tempo, cos� che il circuito possa essere schematizzato come un circuito RL, mentre nella seconda viene usato un segnale $V_{in}$ sinosuidale, il che permette di analizzare il circuito con il formalismo dei fasori.
\subsubsection*{$V_{in}$ costante}
La differenza di potenziale $\Delta V$ ai capi di un'induttanza ideale $L$ � proporzionale alla derivata temporale della corrente: \[ \Delta V(t)= \textit{L} \dot{I}(t). \] L'equazione che descrive il circuito \`e semplicemente \[V_{in}(t) = V_0 = (r + R)I(t) + L\dot{I}(t),\]
da cui si ricava facilmente che 
\begin{equation} \label{eq:tempo_regime}
V_{out}(t)= V_0 \left (\frac{r}{r + R} + \left (1 - \frac{r}{r+R} \right )e^{-\frac{R+r}{L}t} \right).
\end{equation}

\subsubsection*{$V_{in}$ sinosuidale}
Nel caso di un segnale sinusoidale di frequenza $\omega$ immesso nel circuito, svolgendo i conti con le relative impedenze si ha che la funzione di trasferimento risulta essere \[T(\omega) = \frac{r+ j\omega L}{R + r + j\omega L}.\] Chiamando $\omega_T = \frac{r+R}{L}$ si trova che l'attenuazione �
\begin{equation}\label{eq:attenuazione_reale}
A(f) = \left |T(f) \right | =\sqrt{\frac{(\frac{r}{L})^2 + (2 \pi f)^2}{\omega_T ^2 + (2 \pi f)^ 2}},
\end{equation}
mentre lo sfasamento
\begin{equation}\label{eq:sfasamento_reale}
\tan(\Delta \varphi) = \frac{2\pi fR}{r\omega_T + (2 \pi f)^2 L}
\end{equation}
Si noti che nel caso $r \ll R$ (ossia il caso in cui la resistenza interna della componente induttiva risulti trascurabile rispetto alla resistenza del resistore posto in serie), insieme a una delle due condizioni $\omega \sim \omega_T$ o  $\omega \gg \omega_T$, ci si riduce a:\\
\begin{equation} \label{eq:trasferimento_approssimato}
T(\omega) = \frac{1}{1 - j \frac{\omega _T}{\omega}}
\end{equation}
che \`e proprio la funzione di trasferimento di un filtro passa alto. Questo vuol dire (dato che nel nostro caso $r \ll R$ � verificata) che nel nel limite di frequenze non troppo piccole ci aspettiamo un comportamento analogo a quello di un filtro RC passa-alto studiato nelle esperienze precedenti.

\section*{Descrizione delle misure}
Per prima cosa si \`e costruita l'induttanza che ci si proponeva di caratterizzare, dissaldando un toroide di materiale ferromagnetico da una vecchia scheda elettronica e avvolgendo circa $\SI{15}{\meter}$ di filo di rame stagnato ottenendo la componente circuitale in Figura~\ref{fig:Sabrina}.
\begin{figure}
	\centering
	\includegraphics[scale = 0.27]{immagini/Sabrina.jpg}
	\caption{foto dell'induttanza costruita - da noi soprannominata \emph{Sabrina} - che abbiamo deciso di caratterizzare.}
	\label{fig:Sabrina}
\end{figure}
La scelta del valore nominale di $R$ � stata fatta in modo che nel circuito non potessero passare pi� di $\SI{20}{\milli\ampere}$, ovvero la corrente massima erogata dall'Arduino.
Preliminarmente allo studio vero e proprio sono state misurate tutte le resistenze utilizzate con l'uso di un multimetro digitale, ottenendo i valori riportati in Tabella~\ref{tab:dati_resistenze}.

\begin{table}[!ht]
	\centering
	\begin{tabular}{c|c}
		Componente circuitale & Resistenza [$\SI{}{\ohm}$] \\
		\hline \hline
		$R$ & $217\pm2$ \\
		$r$ & $1.6\pm0.3$	\\
	\end{tabular}
	\caption{resistenze utilizzate nel corso dell'esperienza}
	\label{tab:dati_resistenze}
\end{table}
Si \`e dunque proceduti alla calibrazione dell'Arduino attraverso la costruzione di un partitore di tensione, la cui analisi ha permesso di confrontare la misura fornita da un voltmetro con la media delle 256 acquisizioni effettuate dall'Arduino di una stessa differenza di potenziale (assunta costante durante le acquisizioni)\footnote{Lo sketch e lo script Python utilizzati per la calibrazione sono riportati nell'Appendice~\ref{apx:script_calibrazione}}.
Si \`e supposto una relazione lineare tra il valore digitalizzato e la ddp misurata, per cui 
\[
V = \alpha X + \beta,
\]
dove con $X$ si indica il valore acquisito in digit e con $V$ la differenza di potenziale campionata, espressa in Volt. I valori ottenuti sono riportati nella Tabella~\ref{tab:calibrazione}, ed eseguendo un fit iterato con \texttt{curve\_fit}, dopo $4$ iterazioni i parametri convergono ai seguenti valori:
\begin{align*}
\alpha &= (5.040 \pm 0.004)\SI{}{\milli\volt / digit} \\
\beta &= (0.2  \pm 0.1) \SI{}{\milli\volt}\\
\chi ^2 &= 2.70 \\
\text{ndof} &= 11 \\
\text{norm. cov.} &= -0.27 \\
\texttt{absolute\_sigma} &= \texttt{False} \\
\end{align*}
Per una visualizzazione pi\`u immediata della bont\`a del fit si riportano nella Figura~\ref{fig:fit_dati_calibrazione} i punti elencati nella Tabella~\ref{tab:calibrazione}, confrontati con la curva di best fit.
\begin{figure}
	\centering
	\includegraphics[width =\linewidth]{immagini/fit_calibrazione.pdf}
	\caption{curva di best fit in verde confrontata con i punti sperimentali in blu ottenuti per la calibazione dell'Arduino, con relativo grafico dei residui in unit\`a di barre d'errore.}
	\label{fig:fit_dati_calibrazione}
\end{figure}
\begin{table}[!ht]
	\centering
	\begin{tabular}{c|c}
		X [\SI{}{digit}] & V [$\SI{}{\volt}$] \\
		\hline \hline
		$387.12\pm	0.02$ &	$1.95\pm 0.01$ \\
		$4.14 \pm 0.03$ &	$0.0210	0.0002$ \\
		$40.01 \pm	0.02$ &	$0.202 \pm	0.001$ \\
		$60.20 \pm	0.03$ &	$0.306 \pm	0.002$ \\
		$336.98 \pm	0.02$ &	$1.697 \pm 0.009$ \\
		$484.90 \pm	0.02$ & $2.44 \pm 0.01$ \\
		$674.23 \pm	0.03$ & $3.40 \pm 0.02$ \\
		$712.00 \pm	0.02$ &	$3.58 \pm	0.02$ \\
		$787.21 \pm	0.03$ &	$3.97 \pm 0.02$ \\
		$878.94 \pm 0.03$ &	$4.43 \pm 0.02$ \\
		$962.21 \pm 0.04$ &	$4.85 \pm 0.02$ \\
		$1016.83 \pm 0.04$ & $5.13 \pm	0.03$ \\
		$1022.13 \pm 0.04$ & $5.16 \pm	0.03$ \\
	\end{tabular}
	\caption{dati utilizzati per la calibrazione dell'Arduino.}
	\label{tab:calibrazione}
\end{table}

A questo punto si \`e potuti procedere alla prima parte dell'esperienza, nella quale si vuole stimare l'induttanza della componente costruita attraverso la relazione data dall'Equazione~\ref{eq:tempo_regime}.
Montato il circuito come in Figura~\ref{fig:schemacircuito} sono stati eseguiti $30$ campionamenti in diversi istanti di tempo della differenza di potenziale $V_{out}$ (i cui valori digitalizzati sono graficati nella Figura~\ref{fig:fit_dati_regime}) dove l'Arduino era istruito da uno sketch\footnote{Lo sketch e gli script Python utilizzati sono riportati nell'Appendice~\ref{apx:script_carica}} a tenere la porta digitale $7$ al potenziale massimo possibile (nominalmente $\SI{5}{\volt}$). \\
Quindi si \`e proceduti alla seconda parte dell'esperienza in cui si \`e voluta studiare l'attenuazione del filtro costruito: a questo fine sono state misurate le ampiezze picco-picco e gli sfasamenti di onde sinusoidali in entrata e in uscita a varie frequenze, che espolrassero diverse decadi, come mostrato nella Tabella~\ref{tab:dati_bode}. \\
\begin{table}[!ht]
	\centering
	\begin{tabular}{c|c|c|c}
		Frequenza [$\SI{}{\hertz}$] & $V_{in}$ [$\SI{}{\volt}$] & $V_{out}$ [$\SI{}{\volt}$] & $\Delta\varphi \cdot \frac{T}{2\pi}$ [$\SI{}{\second}$] \\ %Sfasamento per unit\`a di periodo su $2\pi$
		\hline \hline 
		$5.544\pm 0.0015$&		$3.204		\pm		0.03$	&	$26.2\cdot 10^{-3}	\pm		5\cdot 10^{-3}$		&	$(5		\pm		5)\si{\milli}$ \\
		$14.72	\pm 0.05$	&		$3.800		\pm		0.03$	&	$33.2\cdot 10^{-3}	\pm 	6.3\cdot 10^{-3}$	&	$5.6	\pm	0.6)\si{\milli}$ \\
		$43.5	\pm 0.5$	 &		$3.800		\pm		0.03$	&	$56.0\cdot 10^{-3}	 \pm	2.8\cdot 10^{-3}$	&	$(3.6	\pm	0.3)\si{\milli}$ \\
		$130.8	 \pm 0.5 $   &		$3.800		\pm		0.03$	&	$142.4\cdot 10^{-3}	\pm	   6.5\cdot 10^{-3}$	&	$(1.52	\pm	0.08)\si{\milli}$ \\
		$243.4	 \pm 0.5$	&		$3.800		\pm		0.03$	&	$258\cdot 10^{-3}	\pm		6\cdot 10^{-3}$		&	$(1.0	\pm	0.03)\si{\milli}$ \\
		$385.2	\pm 0.5$	&		$3.800		\pm		0.03$	&	$408\cdot 10^{-3}	\pm		6\cdot 10^{-3}$		&	$(0.800 \pm   0.06)\si{\milli}$ \\
		$454.6	\pm 0.5$	&		$3.800		\pm		0.03$	&	$480\cdot 10^{-3}	\pm		6\cdot 10^{-3}$		&	$(0.500	\pm	0.03)\si{\milli}$ \\
		$656.4\pm 0.5$		&		$3.820		\pm	 	0.03$	&	$0.680					\pm		 0.006$						&	$(0.336	\pm	0.012)\si{\milli}$ \\
		$853.2 \pm 0.5$		&		$3.820		\pm		0.03$	&	$0.864					\pm		 0.012$	 					&	$(0.248	\pm	0.012)\si{\milli}$ \\
		$1138 	\pm 5$	 	  &		$3.820		\pm		0.03$	&	$1.112		  				\pm	   0.012$						&	$(0.180	\pm	0.03)\si{\milli}$ \\
		$1540\pm 5$			  &		$3.880		\pm		0.03$	&	$1.488					\pm			0.12$					&	$(0.120	\pm	0.004)\si{\milli}$ \\
		$(3.45 \pm 0.05)\si{\kilo}$&		$4.16 \pm 0.06$	&	$2.27					\pm			0.03$					&	$(40	\pm	2)\si{\micro}$ \\
		$(10.16\pm 0.05)\si{\kilo}$&		$4.52 \pm 0.06$	&	$4.12					\pm 		0.06$					&	$(6.4	\pm	1.2)\si{\micro}$ \\
		$(30.10\pm 0.05)\si{\kilo}$&		$4.60 \pm 	0.06$&	$4.52					\pm			0.06$					&	$(0.60	\pm		0.3)\si{\micro}$ \\
	\end{tabular}
	\caption{dati raccolti per studiare la funzione di trasferimento associata al circuito.}
	\label{tab:dati_bode}
\end{table}
Infine sono state fotografate le forme d'onda in uscita dal circuito con segnali variabili in entrata per alcune frequenze, al fine di verificare qualitativamente che il circuito si comportasse come un filtro passa alto, e le immagini sono riportate nell'Appendice~\ref{apx:oscilloscopio}.


\section*{Analisi dei dati}
%TODO: sfasamento
%tenere conto dell'errore di R nel fit
Procediamo all'analisi della prima parte, studiando il tempo caratteristico impiegato dal circuito per arrivare a regime. Si \`e eseguito un fit iterato con la funzione \texttt{curve\_fit} di Python, scegliendo come incertezza sui valori temporali $\Delta t = \SI{4}{\micro\second}$, usando come modello l'Equazione~\ref{eq:tempo_regime}, fissando $R$ al valore riportato in Tabella~\ref{tab:dati_resistenze}. Al fine di tenere debito conto dell'incertezza di misura associata a $R$ invece che eseguire il fit direttamente sui parametri $r$, $V_0$ e $L$ sono stati introdotti $\gamma = \frac{r}{r+R}$ e $\omega_T = \frac{r+R}{L}$ da cui si possono ricavare $r$ ed $L$ come:
\begin{align*}
r &= \frac{\gamma}{1 - \gamma}R \\
L &=\frac{R}{\omega_T(1-\gamma)}
\end{align*}
ove l'errore su $R$ pu\`o essere sommato in quadratura a quelli restituiti dal fit su $\gamma$ e $\omega_T$. Dopo $5$ iterazioni del programma si ottengono i seguenti valori:
\begin{align*}
V_0 &= (5.19 \pm 0.28) \SI{}{\volt}\\
\gamma &= (4.45 \pm 0.39) \cdot 10^{-3} \\
\omega_T &= (23.7\pm 0.5) \SI{}{\kilo\hertz} \\ 
\chi ^2 &= 34.54\\
\text{ndof} &= 27 \\
\text{norm. cov. tra $V_0$ e $\gamma$} &= -0.20\\
\text{norm. cov. tra $V_0$ e $\omega_T$} &= 0.85\\
\text{norm. cov. tra $\gamma$ e $\psi$} &=  0.14\\
\texttt{absolute\_sigma} &= \texttt{False} \\
\end{align*}
che portano a:
\begin{align*}
r &= (0.97\pm 0.07) \SI{}{\ohm}\\
L &=  (9.19 \pm 0.85) \SI{}{\milli\henry}\\
\end{align*}

Per le misure di Arduino \`e stato assunto un errore pari a $1$ \texttt{digit}, opportunamente propagato, sommandolo in quadratura con l'incertezza di calibrazione per ottenere l'errore associato a $V_{in}$ nel fit; il Grafico~\ref{fig:fit_dati_regime} mostra i dati sperimentali in blu sovrapposti alla curva di best fit rappresentata in verde.
La scelta di fissare $R$ al valore tabulato \`e dovuta al fatto che lasciando tutti i parametri liberi alcuni risultano completamente correlati, e tra $r$ ed $R$ quest'ultimo \`e noto con un errore relativo minore rispetto all'altro.\\
\begin{figure}
	\centering
	\includegraphics[width =\linewidth]{immagini/fit_carica_sabrina.pdf}
	\caption{punti sperimentali della risposta del circuito a un potenziale $V_{in}$ costante, confrontati con la curva di best fit e relativi residui (graficati in unit\`a di barre d'errore).}
	\label{fig:fit_dati_regime}
\end{figure}
Per quanto riguarda la seconda parte i dati sperimentali sono stati fittati seguendo il modello descritto dall'Equazione~\ref{eq:attenuazione_reale} poich\`e nella nostra esperienza, nonostante si abbia l'ipotesi $r \ll R$, \`e stato studiato il comportamento del circuito anche per $\omega \ll \omega_T$, per i quali l'approssimazione che porta all'Equazione~\ref{eq:trasferimento_approssimato} non si applica. Il fit iterato eseguito con \texttt{curve\_fit}, sempre sui parametri $\gamma$ e $\omega_T$ invece che direttamente su $r$ ed $L$ (di nuovo per poter tener conto dell'errore su $R$), converge dopo $3$ iterazioni ai seguenti valori per i parametri di best fit:
\begin{align*}
\gamma &= (8.31 \pm 0.67)\cdot 10^{-3}\\
\omega_T &= (22.9 \pm 0.1)\SI{}{\kilo\hertz}\\
\chi ^2 &= 80659.93\\
\text{ndof} &= 11 \\
\text{norm. cov.} &= -0.14\\
\texttt{absolute\_sigma} &= \texttt{False} \\
\end{align*}

da cui

\begin{align*}
r &= (1.82 \pm 0.21) \SI{}{\ohm}\\
L &= (9.57 \pm 0.92) \SI{}{\milli\henry}\\
\end{align*}

Qui come errori sui valori delle frequenze si \`e scelto $1$ \texttt{digit}, come suggerito dal manuale del generatore di frequenze utilizzato. Anche in questo caso si \`e scelto di fissare $R$ al valore tabulato in Tabella~\ref{tab:dati_resistenze}, per le stesse ragioni spiegate nel caso del fit della prima parte.

Sono stati graficati i punti sperimentali (in blu), e confrontati con la curva corrispondente ai parametri di best fit (in verde) nella figura \ref{fig:fit_dati_bode}.
\begin{figure}
	\centering
	\includegraphics[width =\linewidth]{immagini/fit_bode_ampiezze.pdf}
	\caption{Diagramma di bode del circuito studiato, con punti sperimentali in blu confrontati con la curva di best fit, e grafico dei residui in unit\`a di barre d'errore.}
	\label{fig:fit_dati_bode}
\end{figure}

Infine riportiamo per completezza in Figura~\ref{fig:fit_dati_bode_sfa} anche il fit svolto sull'Equazione~\eqref{eq:sfasamento_reale} con \texttt{curve\_fit}. I risultati sono:
\begin{align*}
r &= (1.42 \pm 0.19) \SI{}{\ohm}\\
L &= (8.7 \pm 0.8) \SI{}{\milli\henry}\\
\chi ^2 &= 12.66\\
\text{ndof} &= 11 \\
\text{norm. cov.} &= 0.623\\
\texttt{absolute\_sigma} &= \texttt{False}
\end{align*}
\begin{figure}[h!]
	\centering
	\includegraphics[width =\linewidth]{immagini/fit_bode_sfasamento.pdf}
	\caption{grafico dello sfasamento dell'onda in uscita dalla bobina in funzione della frequenza.}
	\label{fig:fit_dati_bode_sfa}
\end{figure}


\section*{Conclusioni}
Innanzitutto osserviamo che dalle Figure~\ref{fig:Sabrina1}, \ref{fig:Sabrina2} e \ref{fig:Sabrina3}, qualitativamente al crescere della frequenza il segnale in uscita risulta sempre meno distorto rispetto a quello in entrata, in accordo con il comportamento previsto per un filtro passa-alto. Quest'osservazione \`e supportata anche dalla Figura~\ref{fig:fit_dati_bode}, dove osserviamo che l'attenuazione tende a $1$ al crescere della frequenza, mentre lo sfasamento tende a $0$. \\
Tuttavia nel caso di di un'onda quadra in entrata, come nel caso della Figura \ref{fig:Sabrina4}, anche il segnale in ingresso risulta modificato, e questo effetto rimane visibile anche ad alte frequenze, dunque sembra ragionevole dedurne che le impedenze del circuito studiato e quella del generatore d'onda siano confrontabili, per cui i due sistemi non risultano indipendenti. \\
Inoltre dalla Figura~\ref{fig:fit_dati_bode} si evince come il circuito non pu\`o essere considerato un filtro passa alto ideale, in quanto per basse frequenze ($f \ll f_T$) ci si dovrebbe aspettare un'andamento lineare $\frac{f}{f_T}$, che invece non si osserva: la supposizione che tale effetto sia dovuto alla resistenza, piccola, ma non trascurabile, $r$, della bobina trova conferma nel fatto che, includendo $r$ nel modello, l'andamento dei punti sperimentali \`e ben rispecchiato dalla funzione di best-fit, con tutti i punti (tranne due) a meno di una barra d'errrore di distanza dalla curva. Si pu� attribuire al fatto che $r$ non sia trascurabile anche l'effetto per cui lo sfasamento tende a zero per frequenze tendenti a 0, invece che tendere a $\frac{\pi}{2}$, come ci aspetterebbe in un filtro passa-alto. \\
Passando ad analisi pi\`u quantitative osserviamo che i due valori ottenuti per $L$ sono tra di loro compatibili, cos\`i come $V_0$ risulta compatibile con il potenziale fornito in ingresso dalla porta $7$ di Arduino (nominalmente $\SI{5}{\volt})$. Confrontando invece i valori ottenuti per $r$, quello resituito dal secondo fit \`e compatibile con il valore misurato, mentre la stima data dal primo fit non \`e compatibile n\`e con il valore misurato misurato, n\`e con quella data dal secondo fit, e anche i valori di $\gamma$ e $\omega_T$ risultano tra loro incompatibili. \\
Queste incongruenze possono essere dovute a una sottostima delle incertezze di misura, sia dell'Arduino che delle differenze di potenziale picco-picco misurate all'oscilloscopio; un'altra possibile fonte di errore potrebbero essere le impedenze delle altre componenti circuitali collegate, come l'oscilloscopio, l'Arduino o il generatore di funzioni, che si accoppiano a quella del circuito studiato, effetto di cui non si \`e tenuto conto nel modello e che potrebbe essere non trascurabile. Il contributo principale a queste discrepanze \`e probabilmente dovuto al valore di $r$, difficile da misurare a priori e che potrebbe addirittura variare nel corso della presa dei dati come conseguenza di un possibile aumento di temperatura della bobina: al fine di eliminare queste fonti di errore si potrebbe misurare $r$ pi\`u volte, in fasi intermedie dell'esperienza, sviluppare un metodo che diminuisca l'errore associato, possibilmente anche attraverso misure indirette, e provare a misurare la temperatura della componente induttiva durante l'esperimento.

Infine commentiamo a parte i risultati ottenuti dal fit dello sfasamento. Il valore di $L$ risulta compatibile con quelli ottenuti in precedenza, anche se il risultato � stato ottenuto con una precisione minore. Il valore di $r$ \`e invece incompatibile con il risultato restituito dalla stima del tempo caratteristico $RL$, mentre \`e compatibile con quello dato dal secondo fit: questo pu� avvalorare l'ipotesi che $r$ � strettamente legata a fattori ambientali esterni siccome le misure di ampiezze e sfasamenti sono state prese contemporaneamente, a differenza di quelle del circuito $RL$. Questo � anche supportato dal fatto che la misura di $r$ presa con il multimetro � stata fatta poco prima dell'acquisizione dei dati su ampiezze e sfasamenti, ed infatti risulta compatibile con entrambi i valori, ma non con quello ottenuto dal fit sul circuito $RL$, che invece � stato fatto in giorni e luoghi diversi.


\newpage
\appendix 
\section{Codice per calibrare l'Arduino}\label{apx:script_calibrazione}
Per completezza riportiamo di seguito lo sketch caricato su Arduino per eseguire la calibrazione.
\lstinputlisting[style = cpp]{../calibrazione/calibrazione.ino}
Inoltre quello che segue \`e lo script Python usato per la calibrazione dell'Arduino.
\lstinputlisting[style = py]{../calibrazione.py}

\section{Codice per la prima parte dell'esperienza} \label{apx:script_carica}
Anche per quanto riguarda la prima parte dell'esperienza, avendo usato Arduino riportiamo di seguito lo sketch impiegato.
\lstinputlisting[style = cpp]{../carica/carica.ino}
Da computer la prima parte dell'esperienza \`e stata gestita con il seguente script Python:
\lstinputlisting[style = py]{../carica.py}

\section{Studio del filtro passa alto} \label{apx:oscilloscopio}
Riportiamo in questa appendice le foto scattate all'oscilloscpio con onde triangolari o quadre in entrata a varie frequenze.
\begin{figure}[ht!]
	\centering
	\includegraphics[scale = 0.33]{immagini/2_33kHz.jpg}
	\caption{Onda triangolare in entrata e segnale in uscita mostrati dall'oscilloscopio per un segnle entrante di frequenza $f = \SI{2.33}{\kilo\hertz}$.}
	\label{fig:Sabrina1}
\end{figure}
\begin{figure}[ht!]
	\centering
	\includegraphics[scale = 0.33]{immagini/12_27kHz.jpg}
	\caption{Onda triangolare in entrata e segnale in uscita mostrati dall'oscilloscopio per un segnle entrante di frequenza $f = \SI{12.28}{\kilo\hertz}$.}
	\label{fig:Sabrina2}
\end{figure}
\begin{figure}[ht!]
	\centering
	\includegraphics[scale = 0.33]{immagini/31_95kHz.jpg}
	\caption{Onda triangolare in entrata e segnale in uscita mostrati dall'oscilloscopio per un segnle entrante di frequenza $f = \SI{31.95}{\kilo\hertz}$.}
	\label{fig:Sabrina3}
\end{figure}
\begin{figure}[ht!]
	\centering
	\includegraphics[scale = 0.33]{immagini/quadra857Hz.jpg}
	\caption{Onda quadra in entrata e segnale in uscita mostrati dall'oscilloscopio per un segnale entrante di frequenza $f = \SI{857.83}{\hertz}$. La forma distorta dell'onda in ingresso � dovuta al fatto che l'impedenza della resistenza interna del generatore ($r_G=\SI{50}{\ohm}$) non � trascurabile rispetto a quella del circuito della bobina e $R$.}
	\label{fig:Sabrina4}
\end{figure}
\end{document}
