from lab_tool2 import *

filename = 'Elaborato/immagini/fit_calibrazione.pdf'

# Modello
def retta( x, a, b ):
	return a * x + b
	
# Derivata modello
def d_retta( x, a, b):
	return a
	
x, ux, y, uy = numpy.loadtxt('dati_definitivi/calibrazione.txt', unpack = True)
p, cov = MLS(retta, d_retta, x, y, ux, uy, p0 = (0.005, 0.))

print("Parametri: ", p )
print("Chi quadro, ndof: ", chi2MLS(retta, d_retta, p, x, y, ux, uy))
print("Covarianza:\n", cov)
print("Covarianza normalizzata:\n", normCov(cov))
with open('calibrazione_arduino.txt', 'w+') as savefile:
    savefile.write('{0}\t{1}\n'.format(p[0], numpy.sqrt(cov[0][0])))
    
plotter(retta, p, x, y, uy, xlabel = '$V$ [digit]', ylabel = '$V$ [V]', figfile = filename)
