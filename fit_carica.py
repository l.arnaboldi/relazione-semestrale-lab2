
from lab_tool2 import *
from scipy.optimize import curve_fit

datasetnumber = 1# Quale dei 3 campioni usare (0, 1, 2)
filename = 'Elaborato/immagini/fit_carica_sabrina.pdf'

R, dR = numpy.loadtxt('dati_definitivi/R.txt', unpack = True)
cal, dcal = numpy.loadtxt('calibrazione_arduino.txt', unpack = True)

# Parametri
# a = r / (R+r)
# b = (R + r) / L

# Modello
def model(t, V_0, a, b):
    return V_0 * (a + (1 - a) * numpy.exp(-b * t))
    
# Derivata modello
def d_model(t, V_0, a, b):
    return V_0 * (1 - a) * (-b) * numpy.exp(-b * t)

t, y = numpy.loadtxt('dati_definitivi/carica_sabrina_{}.txt'.format(datasetnumber), unpack = True)
t /= 1e6 # Converte da microsecondi a secondi
dt = numpy.array([4e-6] * len(t))
V = y * cal # Converte da unità arduino a Volt
dV= numpy.sqrt(cal**2 + dcal**2 * y**2) # Errore sul voltaggio

initpar = (5., 1.6, 0.009)

p, cov = MLS(model, d_model, x = t, y = V, ux = dt, uy = dV, absolute_sigma = False)
print("Parametri: ", p )
print("Chi quadro, ndof: ", chi2MLS(model, d_model, p, t, V, dt, dV))
print("Covarianza:\n", cov)
print("Covarianza normalizzata:\n", normCov(cov))

plotter(model, p, t, V, dV, dt, d_model, xlabel = '$t$ [s]', ylabel = '$\Delta V$ [V]', figfile = filename)

## Trovo r e L
print('RISULTATI')
print('V0 = ({0} +- {1}) V'.format(p[0], numpy.sqrt(cov[0][0])))
r = p[1] / (1-p[1]) * R
dr = numpy.sqrt(2 * (numpy.sqrt(cov[0][0]) / p[0])**2 + (dR / R)**2) * r
print('r = ({0} +- {1}) Ohm'.format(r, dr))
L = 1 / (p[2] * (1-p[1])) * R
dL = numpy.sqrt((numpy.sqrt(cov[1][1]) / p[1])**2 + (numpy.sqrt(cov[2][2]) / p[2])**2 + (dR / R)**2) * L
print('L = ({0} +- {1}) H'.format(L, dL))
    
    
    
    



