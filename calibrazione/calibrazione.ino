
// Blocco dichiarazioni
const int analogPin = 0; // Definisice la porta A0 usata per la lettura
const int digitalPin = 7; // Definisce la porta 7 usata per la carica
const int points = 256; // Quanti punti sperimentali acquisire

int V[points]; // Definisce l'array intero V
long t[points]; // Definisice l'array t come intero long
long startTime; // Definisce il valore StartTime come intero long 

int start = 0; // Definisce il valore start (usato come flag)
int i; // Definisice la variabile intera i (contatore)

void setup() {
    Serial.begin(19200); // Inizializza la porta seriale a 9600 baud
     
    pinMode(digitalPin, OUTPUT);
    digitalWrite(digitalPin, HIGH);
    
    // Clock digitalizzatore
    bitSet(ADCSRA,ADPS0);
    bitSet(ADCSRA,ADPS1);
    bitSet(ADCSRA,ADPS2);
}
  

void loop() {
    if (Serial.available() > 0) {
        Serial.flush(); // Svuota la seriale
		    start = 1; // Pone il flag start a uno
    } 
      
	  if(!start) return;
                    
    delay(2000); // Delay per essere sicuro che tutto funzioni

    // Pulisce il pin analogico
    for (i = 0; i < 2; i++) {
        V[i]=analogRead(analogPin);
    }
    
    for (i = 0; i < points; i++) {
        V[i]=analogRead(analogPin);
        delay(10); // Aspetta un po' per non prendere le misure tutte nello stesso istante
    }

    // print stampa, printl va anche a capo
    for(i = 0; i < points; i++) {
        Serial.println(V[i]);
    }
    
    start = 0;
}
