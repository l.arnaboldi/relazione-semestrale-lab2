
## IMPOSTAZIONI
PERIOD = 1.e-2
AMPLITUDE = 1.
SPACE_DIVISION = 1000
NTERM = 10000 # numero di termini da considerare nella serie

f_T = 46. # frequanza di taglio

f_TA = 50 # frequenza di taglio integratore es. 4
f_TB = 25e3 # frequenza di taglio derivatore es. 4

f_PA = 10. # frequenza filtro passa alto es. 5
f_5 = 40. #frequenza onda es. 5

## OGGETTI GENERICI
from fourier import *
from matplotlib import pyplot

L2 = L2Space([0., PERIOD])
fourier_sin = SinBase(L2)
fourier_cos = CosBase(L2)
xx = numpy.linspace(-2*L2.domain[1], 2*L2.domain[1], SPACE_DIVISION)
fterm = range(0,NTERM+1)

## ESERCIZIO 1 todo: difefrenze con i valori attesi, bellurie
c_onda_quadra = numpy.array([2/(numpy.pi * n) if n % 2 == 1 else 0. for n in fterm]) * numpy.sqrt( 2*PERIOD ) * AMPLITUDE
def onda_quadra(x):
	return fourier_sin.fcoefficients(c_onda_quadra , x)

c_onda_triangolare = numpy.array([(2/(numpy.pi * n))**2 if n % 2 == 1 else 0. for n in fterm]) * numpy.sqrt( 2*PERIOD ) * AMPLITUDE
def onda_triangolare(x):
	return fourier_cos.fcoefficients(c_onda_triangolare, x)


pyplot.plot(xx, onda_quadra(xx))
pyplot.show()
pyplot.plot(xx, onda_triangolare(xx))
pyplot.show()

## ESERCIZIO 2 todo: confronto sperimentale, bellurie
rapporto = numpy.array( [n / PERIOD / f_T  for n in fterm] )
A = 1/numpy.sqrt( 1. + (rapporto)**2 ) # attenuazione
D_phi = numpy.arctan( -rapporto ) # sfasamento
def w(t, A, D_phi, c_onda_quadra, fsb):
	ans = numpy.zeros(len(t))
	for n in fterm:
		ans += c_onda_quadra[n] * A[n] * fsb.e(n, t, D_phi[n])
	return ans


pyplot.plot(xx, w(xx, A, D_phi, c_onda_quadra, fourier_sin))
pyplot.show()


## ESERCIZIO 3 todo: confronto sperimentale, bellurie
## Nota: l'esecuzione richiede TANTO tempo 
def A(f, freq_taglio = f_T):
	return 1./numpy.sqrt( 1. + (f/freq_taglio)**2)
	
def simulate_A(f, freqT = f_T, deriv = False):
	f_simulation = SinBase(L2Space([0., 1./f]))
	oq = numpy.array([2/(numpy.pi * n) if n % 2 == 1 else 0. for n in fterm]) * numpy.sqrt(2/f) * AMPLITUDE
	rapporto_ = numpy.array( [n*f / freqT  for n in fterm] )
	if deriv:
		rapporto_ = 1./ rapporto_
	A_ = 1/numpy.sqrt( 1. + (rapporto_)**2 ) # attenuazione
	D_phi_ = numpy.arctan( -rapporto_ ) # sfasamento
	yy = w(xx, A_, D_phi_, oq, f_simulation)
	return max(yy) - min(yy)

ff = numpy.logspace(0, 4, SPACE_DIVISION/10) # /10 per non far durare il programma all'infinito
aff = numpy.array([simulate_A(freq) for freq in ff]) / (2*AMPLITUDE)

pyplot.yscale('log')
pyplot.xscale('log')
pyplot.plot(ff, A(ff))
pyplot.plot(ff, aff)
pyplot.show()

## ESERCIZIO 4 todo: tutto

## ESERCIZIO 5 todo: bellurie
fourier_ese5 = SinBase(L2Space([0., 1./f_5]))
xx5 = numpy.linspace(-2./f_5, 2./f_5, SPACE_DIVISION)
oq = numpy.array([2/(numpy.pi * n) if n % 2 == 1 else 0. for n in fterm]) * numpy.sqrt(2/f_5) * AMPLITUDE
rapporto = numpy.array( [f_PA/(n *f_5) if n != 0 else 0. for n in fterm] )
A = 1/numpy.sqrt( 1. + (rapporto)**2 ) # attenuazione
D_phi = numpy.arctan(rapporto) # sfasamento
def w5(t, A, D_phi, c_onda_quadra, fsb):
	ans = numpy.zeros(len(t))
	for n in fterm:
		ans += c_onda_quadra[n] * A[n] * fsb.e(n, t, D_phi[n])
	return ans

pyplot.plot(xx5, w5(xx5, A, D_phi, c_onda_quadra, fourier_ese5))
pyplot.show()



	
	
	
	
	



	






	




